import { useNavigate } from "react-router";
import { useState, useEffect } from "react";
import { useCookies } from "react-cookie";

export default function useFetchData (
    url: string,
    setObject: any,
    headers: any,
    setLoading: (bool: boolean) => void,
    nested: boolean = false,
    ) {
      const navigate = useNavigate();
      useEffect(() => {
        async function fetchNewData() {
          setLoading(true);
          const response = await fetch(url, headers);
          let newData = await response.json();
          if (response.status !== 200) navigate('/');
          if (nested) {
            newData = flattenListOfNestedJsonData(newData)
          };
          if (!newData['detail']) {
              setObject(newData);
          }
          setLoading(false);
        };

        fetchNewData();
          }, []); 
};

export const useDeleteCookie = () => {
  const [cookies, setCookie, removeCookie] = useCookies();
  removeCookie('access_token'); 
};


export const IsAuthenticated = () => {
    
    const [cookies, setCookie, removeCookie] = useCookies();
    const token = cookies.access_token;
    if (!token) {
        return false;
    };
    return true;
};

function flattenListOfNestedJsonData(data: any[]): any[] {
    let result: any[] = [];
    for (const item of data) {
        let flat = flattenJsonData(item);
        result.push(flat);
    }
    return result;
}

function flattenJsonData(data: any, parentKey = ''): any {
    let result: any = {};
    for (const key of Object.keys(data)) {
        let value = data[key];
        if (typeof value === 'object' && !Array.isArray(value)) {
            let flat = flattenJsonData(value, parentKey + key + '.');
            result = { ...result, ...flat };
        } else {
            result[parentKey + key] = value;
        }
    }
    return result;
}
