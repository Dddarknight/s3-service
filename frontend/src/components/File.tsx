export interface File {
  id: number;
  name: string;
  bucket: string;
}
