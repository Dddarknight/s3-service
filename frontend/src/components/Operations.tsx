import * as React from 'react';
import { useState } from "react";
import { useCookies } from "react-cookie";
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import useFetchData from "./Utils";
import { getAuthHeaders } from "./Auth";


const HOST = process.env.REACT_APP_HOST;
const PORT = process.env.REACT_APP_PORT;
const PROTOCOL = process.env.REACT_APP_PROTOCOL;
const URL = `${PROTOCOL}://${HOST}:${PORT}/operations`

interface Column {
  id: string ;
  label: string;
  minWidth?: number;
  align?: 'right';
  format?: (value: number) => string;
}

const columns: readonly Column[] = [
  { id: 'file.name', label: 'File Name', minWidth: 170 },
  { id: 'file.bucket', label: 'Bucket', minWidth: 170 },
  { id: 'operation_type', label: 'Operation Type', minWidth: 170 },
  { id: 'created_at', label: 'Date/time of Operation', minWidth: 170 },
];


interface Operation {
    id: number;
    created_at: Date;
    user_id: number;
    operation_type: string;
    name: string;
    bucket: string;
    [key: string]: string | number | Date;
}

export function GetOperations() {
  const [loading, setLoading] = useState(true);
  const [operations, setOperations] = useState<Operation[]>([]);
  const [cookies, setCookie, removeCookie] = useCookies();
  const token = cookies.access_token;
  const headers = getAuthHeaders(token);

  useFetchData(URL, setOperations, headers, setLoading, true);

  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Paper sx={{ width: '100%', overflow: 'hidden' }}>
      <TableContainer sx={{ maxHeight: 440 }}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align='center'
                  style={{ minWidth: column.minWidth,}}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {!loading
            ? operations
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((operation: Operation) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={operation.id}>
                    {columns.map((column) => {
                      let value = operation[column.id];
                      
                      return (
                        <TableCell key={column.id} align={column.align}>
                          {typeof value === 'number'
                            ? value.toString()
                            : (value instanceof Date
                                ? value.toLocaleString()
                                : value)}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })
              : null}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={operations.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
  );
}