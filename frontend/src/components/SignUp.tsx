import * as React from 'react';
import { useNavigate } from "react-router";
import { useState } from "react";
import axios from "axios";
import Alert from '@mui/material/Alert';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';

const HOST = process.env.REACT_APP_HOST;
const PORT = process.env.REACT_APP_PORT;
const PROTOCOL = process.env.REACT_APP_PROTOCOL;
const URL = `${PROTOCOL}://${HOST}:${PORT}/users`

const theme = createTheme();

export function SignUp() {
  const navigate = useNavigate();
  const [showAlert, setShowAlert] = useState(false);

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const user_data = {
      'username': String(data.get('username')),
      'email': String(data.get('email')),
      'full_name': String(data.get('full_name')),
      'password': String(data.get('password')),
    };
    const headers = {
        'Content-Type': 'application/json'
    }
    
    await axios({
        method: 'post',
        url: URL,
        data: user_data,
        headers: headers
    }).then(response => {
        navigate("/login");
    }).catch(error => {
        setShowAlert(true);
    });
  };

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  autoComplete="username"
                  name="username"
                  required
                  fullWidth
                  id="username"
                  label="username"
                  autoFocus
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="full_name"
                  label="full_name"
                  name="full_name"
                  autoComplete="full_name"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="email"
                  label="email"
                  name="email"
                  autoComplete="email"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="password"
                  label="password"
                  type="password"
                  id="password"
                  autoComplete="password"
                />
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Sign Up
            </Button>
            {showAlert && (
                <Alert severity="error">
                    User with such username/ email is already registered.
                    Try another credentials.
                </Alert>
              )}
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  );
}
