import React from "react";
import { useState, useEffect } from "react";
import { useCookies } from "react-cookie";
import axios from "axios"
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import ListItem from '@mui/material/ListItem';
import TextField from '@mui/material/TextField';
import { getAuthHeaders } from "./Auth";


const HOST = process.env.REACT_APP_HOST;
const PORT = process.env.REACT_APP_PORT;
const PROTOCOL = process.env.REACT_APP_PROTOCOL;
const URL_PROFILE = `${PROTOCOL}://${HOST}:${PORT}/my-profile`
const URL = `${PROTOCOL}://${HOST}:${PORT}/profiles`


export function HandleProfile () {
    const [loading, setLoading] = useState(true);
    const [hasProfile, setHasProfile] = useState(false);
    const [address, setAddress] = useState<string>('');
    const [bio, setBio] = useState<string>('');
    const [id, setId] = useState<string>('');
    const [cookies, setCookie, removeCookie] = useCookies();
    const token = cookies.access_token;
    const headers = getAuthHeaders(token);

    useEffect(() => {
        async function fetchNewData() {
            setLoading(true);
            const response = await fetch(URL_PROFILE, headers);
            let data = await response.json();
            console.log(data.bio);
            setHasProfile(true);
            setBio(data.bio);
            setAddress(data.address);
            setId(data.id)
        };

        fetchNewData();
    }, []); 

    const handleAddressChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
        event.preventDefault();
        setAddress(event.target.value)
    };

    const handleBioChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
        event.preventDefault();
        setBio(event.target.value)
    };

    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        const data = {
            address,
            bio,
        };
        console.log(data);
        console.log(hasProfile);

        if (!hasProfile) {
            await axios.post(URL, data,
            {
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                },
            }
            );
        } else {
            await axios.put(`${URL}/${id}`, data,
            {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
        }        
    };
    return (
    <form onSubmit={handleSubmit}>
        <Box display="flex" flexDirection="column" p={2}>
        <Grid container sx={{width: 1/2,}} columns={2} rowSpacing={2}>
        <Grid item xs={1}>
            <ListItem>Address</ListItem>
        </Grid>
        <Grid item xs={1}>
            <TextField
                id="address"
                value={address}
                onChange={handleAddressChange}
            />
        </Grid>
        <Grid item xs={1}>
        <ListItem>Bio</ListItem>
        </Grid>
        <Grid item xs={1}>
            <TextField
                id="bio"
                value={bio}
                onChange={handleBioChange}
            />
        </Grid>
        </Grid>

        <Button
            type="submit"
            variant="contained"
            color="primary"
            sx={{
                mt: 2,
                md: 2,
                width: 1/4,
            }}
        >
        Submit
        </Button>
        </Box>
    </form>
    );
};
