import * as React from 'react';
import { styled, createTheme, ThemeProvider } from '@mui/material/styles';
import { useNavigate } from "react-router"
import { useCookies } from "react-cookie";
import { useState } from "react";
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import { Link } from 'react-router-dom';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import MuiDrawer from '@mui/material/Drawer';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { GetFiles } from "./Files";
import { GetOperations } from "./Operations";
import { UploadFile } from "./File_Upload";
import { HandleProfile } from "./Profile";
import { IsAuthenticated } from "./Utils";


const drawerWidth: number = 240;

interface AppBarProps extends MuiAppBarProps {
  open?: boolean;
}

interface DashboardContentOptions {
    operations: React.FC;
    files: React.FC;
  }
  
const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})<AppBarProps>(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(['width', 'margin'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
  ({ theme, open }) => ({
    '& .MuiDrawer-paper': {
      position: 'relative',
      whiteSpace: 'nowrap',
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
      boxSizing: 'border-box',
      ...(!open && {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up('sm')]: {
          width: theme.spacing(9),
        },
      }),
    },
  }),
);


const mdTheme = createTheme();


export const Home: React.FC = () => {
    const [open, setOpen] = useState(true);
    const toggleDrawer = () => {
      setOpen(!open);
    };
    const dashboardContentOptions: DashboardContentOptions = {
        operations: GetOperations,
        files: GetFiles,
    };
    
    const ContentComponent: React.FC = () => {
        const navigate = useNavigate();
        const [showFiles, setShowFiles] = useState(false);
        const [showOperations, setShowOperations] = useState(false);
        const [showUpload, setShowUpload] = useState(false);
        const [showProfile, setShowProfile] = useState(false);
        const [cookies, setCookie, removeCookie] = useCookies();
        const logout = async (event: any)=> {
            event.preventDefault();
            removeCookie('access_token'); 
            navigate("/");
        };

        return (
        <ThemeProvider theme={mdTheme}>
            <Box sx={{ display: 'flex' }}>
            <CssBaseline />
            <AppBar position="absolute" open={open}>
                <Toolbar
                sx={{
                    pr: '24px',
                }}
                >
                <Typography
                    component="h1"
                    variant="h6"
                    color="inherit"
                    noWrap
                    sx={{ flexGrow: 1 }}
                >
                    S3-service
                </Typography>
                <Toolbar>
                    {IsAuthenticated() && (
                        <ListItemButton onClick={logout}>
                            <ListItemText primary="Logout" />
                        </ListItemButton>
                    )
                    }
                    {!IsAuthenticated() && (
                        <>
                        <ListItemButton
                            sx={{
                                marginRight: '12px',
                            }}>
                            <Link to="/sign-up" style={{ textDecoration: 'none' }}>
                                <ListItemText primary="SignUp" />
                            </Link>
                        </ListItemButton>
                        <ListItemButton>
                            <Link to="/login" style={{ textDecoration: 'none' }}>
                                <ListItemText primary="Login" />
                            </Link>
                        </ListItemButton>
                        </>
                    )
                    }
                </Toolbar>
                </Toolbar>
            </AppBar>
            <Drawer variant="permanent" open={open}>
                <Toolbar
                sx={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'flex-end',
                    px: [1],
                }}
                >
                <IconButton onClick={toggleDrawer}>
                </IconButton>
                </Toolbar>
                <Divider />
                <List component="nav">
                <React.Fragment>
                    <ListItemButton
                      onClick={(): void => {
                        setShowOperations(false);
                        setShowFiles(true);
                        setShowUpload(false);
                        setShowProfile(false);
                      }}
                    >
                        <ListItemText primary="S3 files" />
                    </ListItemButton>
                    <ListItemButton
                      onClick={(): void => {
                        setShowOperations(false);
                        setShowFiles(false);
                        setShowUpload(true);
                        setShowProfile(false);
                      }}  
                    >
                        <ListItemText primary="Upload file" />
                    </ListItemButton>
                    <ListItemButton
                      onClick={(): void => {
                        setShowOperations(false);
                        setShowFiles(false);
                        setShowUpload(false);
                        setShowProfile(true);
                      }}
                    >
                        <ListItemText primary="My profile" />
                    </ListItemButton>
                    <ListItemButton
                      onClick={(): void => {
                        setShowOperations(true);
                        setShowFiles(false);
                        setShowUpload(false);
                        setShowProfile(false);
                      }}    
                    >
                        <ListItemText primary="My operations" />
                    </ListItemButton>
                    <Divider sx={{ my: 1 }} />
                    </React.Fragment>
                </List>
            </Drawer>
            <Box
                id="box"
                sx={{
                backgroundColor: (theme) =>
                    theme.palette.mode === 'light'
                    ? theme.palette.grey[100]
                    : theme.palette.grey[900],
                flexGrow: 1,
                height: '100vh',
                overflow: 'auto',
                marginTop: '20vh',
                }}
            >
                {showFiles && (
                    <Box
                    component={GetFiles}
                    >
                    </Box>
                )}
                {showOperations && (
                    <Box
                    component={GetOperations}
                    >
                    </Box>
                )}
                {showUpload && (
                    <Box
                    component={UploadFile}
                    >
                    </Box>
                )}
                {showProfile && (
                    <Box
                    component={HandleProfile}
                    >
                    </Box>
                )}
            </Box>
            </Box>
        </ThemeProvider>
        );
    }
    return  (
        <ContentComponent />
      );
}