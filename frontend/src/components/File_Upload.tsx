import React from "react";
import { useNavigate } from "react-router";
import { useState } from "react";
import { useCookies } from "react-cookie";
import axios from "axios";
import Alert from '@mui/material/Alert';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';


const HOST = process.env.REACT_APP_HOST;
const PORT = process.env.REACT_APP_PORT;
const PROTOCOL = process.env.REACT_APP_PROTOCOL;
const URL = `${PROTOCOL}://${HOST}:${PORT}/files`


export function UploadFile () {
    const navigate = useNavigate();
    const [showAlert, setShowAlert] = useState(false);
    const [file, setFile] = useState<File | null>(null);
    const [fileName, setFileName] = useState<string>('');
    const [bucket, setBucket] = useState<string>('');
    const [cookies, setCookie, removeCookie] = useCookies();
    const token = cookies.access_token;

    const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const file = event.target.files![0];
        setFile(file);
        setFileName(file.name);
    };

    const handleBucketChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
        event.preventDefault();
        setBucket(event.target.value)
    };

    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        if (!file) {
            return;
        }
        const data = new FormData();
        data.append('file_obj', file);
        data.append('name', fileName);
        data.append('bucket', bucket);

        await axios({
            method: 'post',
            url: URL,
            data: data,
            headers: {
                Authorization: `Bearer ${token}`,
            },
        }).then(response => {
            setFile(null);
            setFileName('');
            const fileInputElement = document.getElementById("input") as HTMLInputElement;
            if (fileInputElement) {
                fileInputElement.value = '';
                fileInputElement.files = null;
            };
            const inputElement = document.getElementById("bucket") as HTMLTextAreaElement;
            if (inputElement) {
                inputElement.value = '';
            };
        }).catch(error => {
            setShowAlert(true);
        });
    };
    return (
    <form onSubmit={handleSubmit}>
        <Box display="flex" flexDirection="column" p={2}>
        <input
            id="input"
            type="file"
            accept="*"
            onChange={handleFileChange}
        />
        <TextField
            id="bucket"
            label="bucket"
            onChange={handleBucketChange}
            sx={{
                mt: 2,
                md: 2,
                width: 1/4,
            }}
        />
        <Button
            type="submit"
            variant="contained"
            color="primary"
            sx={{
                mt: 2,
                md: 2,
                width: 1/4,
            }}
        >
        Upload
        </Button>
        {showAlert && (
                <Alert severity="error">
                    Something went wrong.
                </Alert>
        )}
        </Box>
    </form>
    );
};
