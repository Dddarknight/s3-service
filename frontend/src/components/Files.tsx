import * as React from 'react';
import { useState } from "react";
import { useCookies } from "react-cookie";
import axios from "axios"
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import TextField from '@mui/material/TextField';
import { File } from "./File";
import useFetchData from "./Utils";
import { getAuthHeaders } from "./Auth";


const HOST = process.env.REACT_APP_HOST;
const PORT = process.env.REACT_APP_PORT;
const PROTOCOL = process.env.REACT_APP_PROTOCOL;
const URL = `${PROTOCOL}://${HOST}:${PORT}/files`


interface Column {
  id: 'name' | 'bucket' | 'download' ;
  label: string;
  minWidth?: number;
  align?: 'right';
  format?: (value: number) => string;
}

export interface Link {
  link: string;
}

const columns: readonly Column[] = [
  { id: 'name', label: 'name', minWidth: 170 },
  { id: 'bucket', label: 'bucket', minWidth: 170 },
  { id: 'download', label: 'download', minWidth: 170 },
];


export function GetFiles() {
  const [loading, setLoading] = useState(true);
  const [files, setFiles] = useState<File[]>([]);
  const [href, setHref] = useState<string>("");
  const [cookies, setCookie, removeCookie] = useCookies();
  const token = cookies.access_token;
  const headers = getAuthHeaders(token);

  useFetchData(URL, setFiles, headers, setLoading);

  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleFilterChange = async (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    let prefix = event.target.value
    const response = await fetch(`${URL}?prefix=${prefix}`, headers);
    let filteredFiles = await response.json();
    setFiles(filteredFiles)
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  

  return (
    <Container>
      <TextField
            id="filter"
            label="filter"
            onChange={handleFilterChange}
            sx={{
                mt: 2,
                md: 2,
                width: 1/4,
            }}
      />
      <TableContainer sx={{ maxHeight: 440 }}>
        <Table>
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align='center'
                  style={{ minWidth: column.minWidth,}}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {!loading
            ? files
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((file: File) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={file.id}>
                    {columns.map((column) => {
                      if (column.id == 'download') {
                        const url = `${URL}/${file.id}`
                        return (
                            <TableCell key={column.id} align={column.align}>
                            <Button onClick=
                            {async () => {
                                axios.get(url, headers
                                ).then(function (response) {
                                    const data = response.data;
                                    setHref(data.link);
                                    window.location.href = data.link;
                                });
                            }}
                            >Download</Button>
                            </TableCell>
                        );
                      } else {
                        const value = file[column.id];
                        return (
                            <TableCell key={column.id} align={column.align}>
                            {column.format && typeof value === 'number'
                                ? column.format(value)
                                : value}
                            </TableCell>
                        );
                      };
                    })}
                  </TableRow>
                );
              })
              : null}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={files.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Container>
  );
}