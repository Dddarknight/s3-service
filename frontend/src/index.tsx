import React from "react";
import * as ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import { Home } from "./components/Home";
import { GetFiles } from "./components/Files";
import { GetOperations } from "./components/Operations";
import { Login } from './components/Login';
import { SignUp } from "./components/SignUp";
import { UploadFile } from "./components/File_Upload";
import { IsAuthenticated } from "./components/Utils";
import { HandleProfile } from "./components/Profile";


function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/sign-up" element={<SignUp />} />
        <Route
          path="/files"
          element={
            IsAuthenticated() ? (
              <GetFiles />
            ) : (
            <Navigate replace to={"/login"} />
            )
          } />
        <Route
          path="/operations"
          element={
            IsAuthenticated() ? (
              <GetOperations />
            ) : (
            <Navigate replace to={"/login"} />
            )
          } />
        <Route
          path="/file-upload"
          element={
            IsAuthenticated() ? (
              <UploadFile />
            ) : (
            <Navigate replace to={"/login"} />
            )
          } />
        <Route
          path="/profile"
          element={
            IsAuthenticated() ? (
              <HandleProfile />
            ) : (
            <Navigate replace to={"/login"} />
            )
          } />
      </Routes>
    </BrowserRouter>
  );
}

const root = ReactDOM.createRoot(document.getElementById("root")!);
root.render(<App />);
