# s3-service


## Description
The app provides the opportunity to upload, download files from S3 storage and get the information about S3 objects and users' operations.

The app consists of backend API, frontend and tasks.

**API service**

- API service is based on FastAPI and Dependency Injector.
- All processes are running asynchronously, including requests to database (PostgreSQL).
- There are 2 variants of users' authentication: simple and JWT authentication. JWT authentication is provided by default (default value of OAUTH2 environment variable is 'JWT').

- Endpoints:


| Endpoint | Method | Description |
|----------|---------|---------|
| /users | GET |  Returns all users. |
| /users | POST |  Creates a user. |
| /users/{user_id} | GET |  Gets a user by id. |
| /users/{user_id} | DELETE |  Deletes a user. |
| /token | POST |  Retuns a token. |
| /profiles | GET |  Returns all profiles. |
| /profiles | POST |  Creates a profile. |
| /profiles/{profile_id} | GET |  Gets a profile by id. |
| /profiles/{profile_id} | PUT |  Updates a profile. |
| /profiles/{profile_id} | DELETE |  Deletes a profile. |
| /my-profile | GET |  Gets a profile of the current user. |
| /files<?prefix=> | GET |  Returns all files. If the prefix is given, returns filtered files. |
| /files | POST |  Uploads a file. |
| /files/{file_id} | GET |  Downloads a file. |
| /operations | GET |  Returns all current user's operations. |

***Tasks***

- Tasks service is implemented with Celery.
- It downloads all objects from S3 storage asynchronously with asyncio.
- All files are stored in zip archive. If you use Docker, to get files from the container, if necessary, please run:
```
$ docker cp celery_worker:/s3_files.zip .

```
**Frontend**

- Frontend is implemented with React. It is making requests to API service to get information.
- To work with files and see your operaions, you need to sign up and login first.
- On the main dashboard you can see/ update your profile data, upload new file to the S3 storage, see all files in the S3 storage and download them, see all your operations with files. The table with files contains filter to search files with the given prefix. The table of operaions contains information about file_name, bucket, operation type (load/ upload), date/ time of the operation.


## Installation
**Copy a project**
```
$ git clone git@gitlab.com:Dddarknight/s3-service.git
$ cd s3-service 
```

**Working with Docker**

***Set up environment variables***

First, you have to set up your environment variables in docker-compose.yml (BOTO_KEY_ID, BOTO_ACCESS_KEY - for S3 storage, SECRET_KEY). Change S3_ENDPOINT_URL, if necessary. To get SECRET_KEY, run:
``` 
$ openssl rand -hex 32
```
***Launch***

```
$ docker compose up --build

# Go to 0.0.0.0:3000
```

**Working without Docker**

***Set up environment variables***

Backend:
``` 
$ touch .env

# You have to fill .env file. See .env.example. You will have to fill 
#1) credentials for PostgreSQL. If you don't have these credentials, please follow the instructions in the official documentation.
#2) KEY_ID and ACCESS_KEY for your S3 storage, and S3_ENDPOINT_URL, if necessary (the default value is for Yandex S3).
#3) SECRET_KEY for FastAPI. To get it, run:

$ openssl rand -hex 32
```

Frontend:
```
$ cd frontend
$ touch .env
# Then you have to fill .env file in frontend. See .env.example.

$ cd ..
```


***Launch without Docker***
```

# Backend (from s3-service directory):
$ cd backend
$ pip install poetry
$ poetry install

# Then you have to create 's3' database in PostgreSQL.

$ make run

# Frontend (from s3-service directory):
$ cd frontend
$ npm install
$ npm start


# Launch Celery scheduled tasks
$ cd backend
$ celery -A celery_tasks beat --loglevel=info

# Launch the Celery worker
$ cd backend
$ celery -A celery_tasks worker --loglevel=info --pool solo

```


## Tools
This project was built using these tools:
| Tool | Description |
|----------|---------|
| [FastAPI](https://fastapi.tiangolo.com/) | "Web framework for building APIs with Python" |
| [Dependency Injector](https://python-dependency-injector.ets-labs.org/index.html) | "A dependency injection framework for Python" |
| [PostgreSQL](https://www.postgresql.org/) |  "An open source object-relational database system" |
| [SQLAlchemy](https://www.sqlalchemy.org/) |  "The Python SQL toolkit and Object Relational Mapper" |
| [Alembic](https://alembic.sqlalchemy.org/en/latest/) |  "A lightweight database migration tool for usage with the SQLAlchemy" |
| [React](https://reactjs.org/) |  "A JavaScript library for building user interfaces" |
| [Celery](https://docs.celeryq.dev/en/stable/index.html) | "A task queue with focus on real-time processing, while also supporting task scheduling" |
| [poetry](https://python-poetry.org/) |  "Python dependency management and packaging made easy" |
| [Py.Test](https://pytest.org) | "A mature full-featured Python testing tool" |


## Contributing
Open to contributions!


## License
[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)

## Project status
Development
