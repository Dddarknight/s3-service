import asynctest
import pytest
from dependency_injector import providers
from fastapi.testclient import TestClient
from httpx import AsyncClient
from unittest import mock

from src.config import settings
from src.database import Database
from src.server import app
from src.users.repositories import UserRepository, UserNotFoundError
from src.users.models import User
from tests.utils import get_test_data


test_data_users = get_test_data('users.json')


@pytest.fixture
def client():
    yield TestClient(app)


def test_get_list(client):
    user_1 = test_data_users['users_list']['user1']
    user_2 = test_data_users['users_list']['user2']

    repository_mock = mock.Mock(spec=UserRepository)
    repository_mock.get_all_users.return_value = [
        User(**user_1),
        User(**user_2),
    ]

    with app.container.user_repository.override(repository_mock):
        response = client.get("/users")

    assert response.status_code == 200
    data = response.json()
    assert data == [
        user_1,
        user_2,
    ]


def test_get_by_id(client):
    user_1 = test_data_users['users_list']['user1']
    repository_mock = mock.Mock(spec=UserRepository)
    repository_mock.get_user_by_id.return_value = User(
        **user_1
    )

    with app.container.user_repository.override(repository_mock):
        response = client.get("/users/1")

    assert response.status_code == 200
    data = response.json()
    assert data == user_1
    repository_mock.get_user_by_id.assert_called_once_with(1)


def test_get_by_id_404(client):
    repository_mock = mock.Mock(spec=UserRepository)
    repository_mock.get_user_by_id.side_effect = UserNotFoundError(1)

    with app.container.user_repository.override(repository_mock):
        response = client.get("/users/1")

    assert response.status_code == 404


def test_create(client):
    user_1 = test_data_users['users_create']['user1']
    repository_mock = mock.Mock(spec=UserRepository)
    repository_mock.create_user.return_value = User(**user_1)
    repository_mock.get_user_by_username.return_value = None
    repository_mock.get_user_by_email.return_value = None

    user_data = test_data_users['users_post']['user1']
    with app.container.user_repository.override(repository_mock):
        response = client.post("/users", json=user_data)

    assert response.status_code == 201
    data = response.json()
    assert data == test_data_users['users_list']['user1']


def test_delete(client):
    user_mock = mock.Mock()
    user_mock.id = 1
    repository_mock = mock.Mock(spec=UserRepository)

    with app.container.user_repository.override(
        repository_mock
    ), app.container.current_token.override(
        asynctest.CoroutineMock(return_value="token")
    ), app.container.current_user.override(
        asynctest.CoroutineMock(return_value=user_mock)
    ):
        response = client.delete("/users/1")
        assert response.status_code == 204
        repository_mock.delete_user.assert_called_once_with(1)
        response = client.delete("/users/2")
        assert response.status_code == 404


@pytest.fixture
def async_client():
    yield AsyncClient(app=app, base_url="http://localhost:8000")


@pytest.mark.asyncio
async def test_create_user(async_client):
    test_db = providers.Singleton(Database, db_url=settings.TEST_DB)
    with app.container.db.override(test_db):
        db = app.container.db()
        await db.create_database()
        data = test_data_users["users_post"]["user1"]
        user = await async_client.post('/users',
                                       json=data)
        assert user.json()['username'] == data['username']
        assert user.json()['email'] == data['email']
        assert user.json()['full_name'] == data['full_name']
        await db.drop_database()


@pytest.mark.asyncio
async def test_get_user(async_client):
    test_db = providers.Singleton(Database, db_url=settings.TEST_DB)
    with app.container.db.override(test_db):
        db = app.container.db()
        await db.create_database()
        data = test_data_users["users_post"]["user1"]
        created_user = await async_client.post('/users', json=data)
        user = await async_client.get(
            f'/users/{created_user.json().get("id")}')
        assert user.json()['username'] == data['username']
        assert user.json()['email'] == data['email']
        assert user.json()['full_name'] == data['full_name']
        await db.drop_database()


@pytest.mark.asyncio
async def test_delete_user(async_client):
    test_db = providers.Singleton(Database, db_url=settings.TEST_DB)
    with app.container.db.override(test_db):
        db = app.container.db()
        await db.create_database()

        data_user_1 = test_data_users["users_post"]["user1"]
        created_user_1 = await async_client.post('/users', json=data_user_1)
        id_1 = created_user_1.json().get("id")
        data_user_2 = test_data_users["users_post"]["user2"]
        created_user_2 = await async_client.post('/users', json=data_user_2)
        id_2 = created_user_2.json().get("id")

        form_data = {
            'username': test_data_users["users_post"]["user1"]["username"],
            'password': test_data_users["users_post"]["user1"]["password"],
            'scope': 'a b',
        }
        token_response = await async_client.post('/token', data=form_data)
        token = token_response.json().get('access_token')

        response = await async_client.delete(
            f'/users/{id_2}',
            headers={"Authorization": f"Bearer {token}"})
        assert response.status_code == 404
        response = await async_client.delete(
            f'/users/{id_1}',
            headers={"Authorization": f"Bearer {token}"})
        assert response.status_code == 204
        response = await async_client.get(f'/users/{id_1}')
        assert response.status_code == 404
        await db.drop_database()
