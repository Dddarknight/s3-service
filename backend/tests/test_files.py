import asynctest
import pytest
from dependency_injector import providers
from fastapi.testclient import TestClient
from httpx import AsyncClient
from mypy_boto3_s3 import S3Client
from unittest import mock

from src.config import settings
from src.database import Database
from src.server import app
from src.files.models import File, Operation
from src.files.repositories import FileRepository
from tests.utils import get_test_data, get_fixture_path, get_test_token


test_data_files = get_test_data('files.json')
test_data_operations = get_test_data('operations.json')

file_1 = test_data_files['files_list']['file1']
file_2 = test_data_files['files_list']['file2']


@pytest.fixture
def client():
    yield TestClient(app)


def test_get_files_list(client):
    repository_mock = mock.Mock(spec=FileRepository)
    repository_mock.get_files.return_value = [
        File(**file_1),
        File(**file_2),
    ]
    user_mock = mock.Mock()

    with app.container.file_repository.override(repository_mock):
        response = client.get("/files")
        assert response.status_code == 401
        with app.container.current_token.override(
                asynctest.CoroutineMock(return_value="token")
        ), app.container.current_user.override(
                asynctest.CoroutineMock(return_value=user_mock)
        ):
            response = client.get("/files")
            assert response.status_code == 200
            data = response.json()
            assert data == [
                file_1,
                file_2,
            ]


def test_get_operations_list(client):
    operation_1 = test_data_operations['operations_list']['operation1']
    operation_2 = test_data_operations['operations_list']['operation2']

    repository_mock = mock.Mock(spec=FileRepository)
    repository_mock.get_operations.return_value = [
        Operation(
            id=operation_1['id'],
            user_id=operation_1['user_id'],
            file=File(**file_1),
            created_at=operation_1['created_at'],
            operation_type=operation_1['operation_type']
        ),
        Operation(
            id=operation_2['id'],
            user_id=operation_2['user_id'],
            file=File(**file_2),
            created_at=operation_2['created_at'],
            operation_type=operation_2['operation_type']
        )
    ]
    user_mock = mock.Mock()

    with app.container.file_repository.override(repository_mock):
        response = client.get("/operations")
        assert response.status_code == 401
        with app.container.current_token.override(
                asynctest.CoroutineMock(return_value="token")
        ), app.container.current_user.override(
                asynctest.CoroutineMock(return_value=user_mock)
        ):
            response = client.get("/operations")
            assert response.status_code == 200
            data = response.json()
            assert data == [
                operation_1,
                operation_2,
            ]


def test_load_file(client):
    operation_1 = test_data_operations['operations_list']['operation1']
    repository_mock = mock.Mock(spec=FileRepository)
    repository_mock.load_file.return_value = Operation(
        id=operation_1['id'],
        user_id=operation_1['user_id'],
        file=File(**file_1),
        created_at=operation_1['created_at'],
        operation_type=operation_1['operation_type']
    )
    repository_mock.get_file_by_id.return_value = File(**file_1)

    service_mock = mock.Mock(spec=S3Client)
    service_mock.generate_presigned_url.return_value = 'presigned_url'

    user_mock = mock.Mock()
    user_mock.id = 1

    with app.container.file_repository.override(
        repository_mock
    ), app.container.s3_client.override(service_mock):
        response = client.get("/files/1")
        assert response.status_code == 401
        with app.container.current_token.override(
            asynctest.CoroutineMock(return_value="token")
        ), app.container.current_user.override(
            asynctest.CoroutineMock(return_value=user_mock)
        ):
            response = client.get("/files/1")
            assert response.status_code == 200
            data = response.json()
            assert data == {'link': 'presigned_url'}


def test_upload_file(client):
    repository_mock = mock.Mock(spec=FileRepository)
    repository_mock.upload_file.return_value = ''

    service_mock = mock.Mock(spec=S3Client)
    service_mock.generate_presigned_url.return_value = 'presigned_url'

    user_mock = mock.Mock()
    user_mock.id = 1

    with app.container.file_repository.override(
        repository_mock
    ), app.container.s3_client.override(service_mock):
        file_obj = open(get_fixture_path('files.json'), 'rb')
        response = client.post(
            "/files",
            data={
                'name': file_1['name'],
                'bucket': file_1['bucket'],
            },
            files=[('file_obj', file_obj)],
        )
        file_obj.close()
        assert response.status_code == 401
        with app.container.current_token.override(
            asynctest.CoroutineMock(return_value="token")
        ), app.container.current_user.override(
            asynctest.CoroutineMock(return_value=user_mock)
        ):
            response = client.post(
                "/files",
                data={
                    'name': file_1['name'],
                    'bucket': file_1['bucket'],
                },
                files=[('file_obj', open(
                    get_fixture_path('files.json'), 'rb'))],
            )
            assert response.status_code == 201
            data = response.json()
            assert data == {'link': 'presigned_url'}


@pytest.fixture
def async_client():
    yield AsyncClient(app=app, base_url="http://localhost:8000")


@pytest.mark.asyncio
async def test_files_with_db(async_client):
    test_db = providers.Singleton(Database, db_url=settings.TEST_DB)
    service_mock = mock.Mock(spec=S3Client)
    service_mock.generate_presigned_url.return_value = 'presigned_url'
    service_mock.upload_fileobj.return_value = ''

    with app.container.db.override(
            test_db), app.container.s3_client.override(service_mock):
        db = app.container.db()
        await db.create_database()
        token_1 = await get_test_token(async_client, "user1")

        file_data_1 = test_data_files['files']['file1']
        file_obj = open(get_fixture_path('files.json'), 'rb')
        await async_client.post(
            '/files',
            data={
                'name': file_data_1['name'],
                'bucket': file_data_1['bucket'],
            },
            files=[('file_obj', file_obj)],
            headers={"Authorization": f"Bearer {token_1}"}
        )

        token_2 = await get_test_token(async_client, "user2")

        file_data_2 = test_data_files['files']['file2']
        await async_client.post(
            '/files',
            data={
                'name': file_data_2['name'],
                'bucket': file_data_2['bucket'],
            },
            files=[('file_obj', file_obj)],
            headers={"Authorization": f"Bearer {token_2}"}
        )
        file_obj.close()

        files = await async_client.get(
            '/files',
            headers={"Authorization": f"Bearer {token_1}"}
        )
        assert str.encode(file_data_1['name']) in files.content
        assert str.encode(file_data_1['bucket']) in files.content
        assert str.encode(file_data_2['name']) in files.content
        assert str.encode(file_data_2['bucket']) in files.content

        operations = await async_client.get(
            '/operations',
            headers={"Authorization": f"Bearer {token_1}"}
        )
        assert str.encode(file_data_1['name']) in operations.content
        assert str.encode(file_data_1['bucket']) in operations.content
        assert str.encode(file_data_2['name']) not in operations.content
        assert str.encode(file_data_2['bucket']) not in operations.content
        assert str.encode('UPLOAD') in operations.content

        await async_client.get(
            '/files/2',
            headers={"Authorization": f"Bearer {token_1}"}
        )
        operations = await async_client.get(
            '/operations',
            headers={"Authorization": f"Bearer {token_1}"}
        )
        assert str.encode(file_data_1['name']) in operations.content
        assert str.encode(file_data_1['bucket']) in operations.content
        assert str.encode(file_data_2['name']) in operations.content
        assert str.encode(file_data_2['bucket']) in operations.content

        await db.drop_database()
