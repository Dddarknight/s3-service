import json
import os


FIXTURES_DIR_NAME = 'fixtures'


def get_fixture_path(file_name):
    current_dir = os.path.dirname(os.path.abspath(__file__))
    return os.path.join(current_dir, FIXTURES_DIR_NAME, file_name)


def read(file_path):
    with open(file_path, 'r') as file:
        content = file.read()
    return content


def get_fixture_data(file_name):
    content = read(get_fixture_path(file_name))
    return json.loads(content)


def get_test_data(file_name):
    return get_fixture_data(file_name)


async def get_test_token(async_client, user):
    test_data_users = get_test_data('users.json')
    user_data = test_data_users["users_post"][user]
    await async_client.post('/users', json=user_data)

    form_data = {
        'username': test_data_users["users_post"][user]["username"],
        'password': test_data_users["users_post"][user]["password"],
        'scope': 'a b',
    }
    token_response = await async_client.post('/token', data=form_data)
    token = token_response.json().get('access_token')
    return token
