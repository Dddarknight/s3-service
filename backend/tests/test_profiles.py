import asynctest
import pytest
from dependency_injector import providers
from fastapi.testclient import TestClient
from httpx import AsyncClient
from unittest import mock

from src.config import settings
from src.database import Database
from src.server import app
from src.profiles.models import Profile
from src.profiles.repositories import (
    ProfileRepository, ProfileNotFoundError
)
from tests.utils import get_test_data, get_test_token


test_data_profiles = get_test_data('profiles.json')
test_data_users = get_test_data('users.json')


@pytest.fixture
def client():
    yield TestClient(app)


def test_get_list(client):
    profile_1 = test_data_profiles['profiles_list']['profile1']
    profile_2 = test_data_profiles['profiles_list']['profile2']

    repository_mock = mock.Mock(spec=ProfileRepository)
    repository_mock.get_all_profiles.return_value = [
        Profile(**profile_1),
        Profile(**profile_2),
    ]

    with app.container.profile_repository.override(repository_mock):
        response = client.get("/profiles")

    assert response.status_code == 200
    data = response.json()
    assert data == [
        profile_1,
        profile_2,
    ]


def test_get_by_id(client):
    profile_1 = test_data_profiles['profiles_list']['profile1']
    repository_mock = mock.Mock(spec=ProfileRepository)
    repository_mock.get_profile_by_id.return_value = Profile(
        **profile_1
    )

    with app.container.profile_repository.override(repository_mock):
        response = client.get("/profiles/1")

    assert response.status_code == 200
    data = response.json()
    assert data == profile_1
    repository_mock.get_profile_by_id.assert_called_once_with(1)


def test_get_my_profile(client):
    profile_1 = test_data_profiles['profiles_list']['profile1']
    repository_mock = mock.Mock(spec=ProfileRepository)
    repository_mock.get_profile_by_user_id.return_value = Profile(
        **profile_1
    )
    user_mock = mock.Mock()
    user_mock.id = 1

    with app.container.profile_repository.override(
        repository_mock
    ):
        response = client.get("/my-profile")
        assert response.status_code == 401
        with app.container.current_token.override(
            asynctest.CoroutineMock(return_value="token")
        ), app.container.current_user.override(
            asynctest.CoroutineMock(return_value=user_mock)
        ):
            response = client.get("/my-profile")
            assert response.status_code == 200
            data = response.json()
            assert data == profile_1


def test_get_by_id_404(client):
    repository_mock = mock.Mock(spec=ProfileRepository)
    repository_mock.get_profile_by_id.side_effect = ProfileNotFoundError(1)

    with app.container.profile_repository.override(repository_mock):
        response = client.get("/profiles/1")

    assert response.status_code == 404


def test_create(client):
    profile_1 = test_data_profiles['profiles_list']['profile1']
    repository_mock = mock.Mock(spec=ProfileRepository)
    repository_mock.create_profile.return_value = Profile(**profile_1)

    user_mock = mock.Mock()
    user_mock.id = 1

    profile_data = test_data_profiles['profiles']['profile1']
    with app.container.profile_repository.override(repository_mock):
        response = client.post("/profiles", json=profile_data)
        assert response.status_code == 401
        with app.container.current_token.override(
            asynctest.CoroutineMock(return_value="token")
        ), app.container.current_user.override(
            asynctest.CoroutineMock(return_value=user_mock)
        ):
            response = client.post("/profiles", json=profile_data)
            assert response.status_code == 201
            data = response.json()
            assert data == test_data_profiles['profiles_list']['profile1']


def test_update(client):
    profile_1 = test_data_profiles['profiles_list']['profile1']
    repository_mock = mock.Mock(spec=ProfileRepository)
    repository_mock.update_profile.return_value = Profile(**profile_1)
    repository_mock.get_profile_by_id.return_value = Profile(**profile_1)

    user_mock = mock.Mock()
    user_mock.id = 1

    profile_data = test_data_profiles['profiles']['profile1']
    with app.container.profile_repository.override(repository_mock):
        response = client.put("/profiles/1", json=profile_data)
        assert response.status_code == 401
        with app.container.current_token.override(
            asynctest.CoroutineMock(return_value="token")
        ), app.container.current_user.override(
            asynctest.CoroutineMock(return_value=user_mock)
        ):
            response = client.put("/profiles/1", json=profile_data)
            assert response.status_code == 200
            data = response.json()
            assert data == test_data_profiles['profiles_list']['profile1']


def test_delete(client):
    user_mock = mock.Mock()
    user_mock.profile.id = 1
    repository_mock = mock.Mock(spec=ProfileRepository)

    with app.container.profile_repository.override(
            repository_mock), app.container.current_token.override(
                asynctest.CoroutineMock(return_value="token")
            ), app.container.current_user.override(
                asynctest.CoroutineMock(return_value=user_mock)
            ):
        response = client.delete("/profiles/1")
        assert response.status_code == 204
        repository_mock.delete_profile.assert_called_once_with(1)
        response = client.delete("/profiles/2")
        assert response.status_code == 404


@pytest.fixture
def async_client():
    yield AsyncClient(app=app, base_url="http://localhost:8000")


@pytest.mark.asyncio
async def test_create_get_profile(async_client):
    test_db = providers.Singleton(Database, db_url=settings.TEST_DB)
    with app.container.db.override(test_db):
        db = app.container.db()
        await db.create_database()
        token_1 = await get_test_token(async_client, "user1")

        profile_data_1 = test_data_profiles['profiles']['profile1']
        profile_1 = await async_client.post(
            '/profiles',
            json=profile_data_1,
            headers={"Authorization": f"Bearer {token_1}"}
        )
        assert profile_1.json()['address'] == profile_data_1['address']
        assert profile_1.json()['bio'] == profile_data_1['bio']
        id = profile_1.json()['id']

        profile = await async_client.get(f'/profiles/{id}')
        assert profile.json()['address'] == profile_data_1['address']
        assert profile.json()['bio'] == profile_data_1['bio']

        token_2 = await get_test_token(async_client, "user2")

        profile_data_2 = test_data_profiles['profiles']['profile2']
        await async_client.post(
            '/profiles',
            json=profile_data_2,
            headers={"Authorization": f"Bearer {token_2}"}
        )
        profiles = await async_client.get('/profiles')
        assert str.encode(profile_data_1['address']) in profiles.content
        assert str.encode(profile_data_1['bio']) in profiles.content
        assert str.encode(profile_data_2['address']) in profiles.content
        assert str.encode(profile_data_2['bio']) in profiles.content
        await db.drop_database()


@pytest.mark.asyncio
async def test_update_delete_profile(async_client):
    test_db = providers.Singleton(Database, db_url=settings.TEST_DB)
    with app.container.db.override(test_db):
        db = app.container.db()
        await db.create_database()
        token_1 = await get_test_token(async_client, "user1")
        token_2 = await get_test_token(async_client, "user2")

        profile_data_1 = test_data_profiles['profiles']['profile1']
        profile_1 = await async_client.post(
            '/profiles',
            json=profile_data_1,
            headers={"Authorization": f"Bearer {token_1}"}
        )
        id_1 = profile_1.json().get('id')
        profile_data_2 = test_data_profiles['profiles']['profile2']
        profile_2 = await async_client.post(
            '/profiles',
            json=profile_data_2,
            headers={"Authorization": f"Bearer {token_2}"}
        )
        id_2 = profile_2.json().get('id')

        response = await async_client.put(
            f'/profiles/{id_2}',
            json=profile_data_1,
            headers={"Authorization": f"Bearer {token_1}"}
        )
        assert response.status_code == 404
        profile_1_updated = await async_client.put(
            f'/profiles/{id_1}',
            json=profile_data_2,
            headers={"Authorization": f"Bearer {token_1}"}
        )
        assert profile_1_updated.json()['address'] == profile_data_2['address']
        assert profile_1_updated.json()['bio'] == profile_data_2['bio']

        response = await async_client.delete(
            f'/profiles/{id_2}',
            headers={"Authorization": f"Bearer {token_1}"}
        )
        assert response.status_code == 404
        response = await async_client.delete(
            f'/profiles/{id_1}',
            headers={"Authorization": f"Bearer {token_1}"}
        )
        assert response.status_code == 204
        response = await async_client.get(f'/profiles/{id_1}')
        assert response.status_code == 404
        await db.drop_database()
