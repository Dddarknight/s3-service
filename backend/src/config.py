import os
from pydantic import BaseSettings, PostgresDsn, RedisDsn
from dotenv import load_dotenv


load_dotenv()


class Config(BaseSettings):
    DATABASE_URL: PostgresDsn = os.getenv('DATABASE_URL')

    CORS_ORIGINS: list[str] = os.getenv('CORS_ORIGINS')
    CORS_HEADERS: list[str] = os.getenv('CORS_HEADERS')

    BOTO_KEY_ID: str = os.getenv('BOTO_KEY_ID')
    BOTO_ACCESS_KEY: str = os.getenv('BOTO_ACCESS_KEY')
    S3_ENDPOINT_URL: str = os.getenv('S3_ENDPOINT_URL')

    JWT_ALGORITHM: str = os.getenv('JWT_ALGORITHM')
    OAUTH2_METHOD: str = os.getenv('OAUTH2_METHOD')
    SECRET_KEY: str = os.getenv('SECRET_KEY')
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 360

    FILE_LOAD_LINK_EXPIRE_TIME: int = 3600

    TEST_DB: PostgresDsn = os.getenv('TEST_DB')

    MODE: str = os.getenv('MODE')
    HOST: str = os.getenv('HOST')

    BROKER_URL: RedisDsn = os.getenv('BROKER_URL')
    PERIOD_FOR_CELERY_TASK: int = 2
    FILES_DIRECTORY: str = 's3_files'


settings = Config()
