from contextlib import AbstractContextManager
from typing import Callable, Iterator

from sqlalchemy import select, delete
from sqlalchemy.ext.asyncio import AsyncSession

from src.exceptions import NotFoundError
from src.profiles import schemas
from src.profiles.models import Profile


class ProfileRepository:

    def __init__(
        self,
        session_factory: Callable[..., AbstractContextManager[AsyncSession]]
    ) -> None:
        self.session_factory = session_factory

    async def get_all_profiles(self) -> Iterator[Profile]:
        async with self.session_factory() as session:
            async with session.begin():
                profiles = await session.scalars(select(Profile))
                return profiles.all()

    async def get_profile_by_id(self, profile_id: int) -> Profile:
        async with self.session_factory() as session:
            async with session.begin():
                query = await session.execute(select(Profile).filter_by(
                    id=profile_id).limit(1))
                profile = query.scalars().first()
                if not profile:
                    raise ProfileNotFoundError(profile_id)
                return profile

    async def get_profile_by_user_id(self, user_id: int) -> Profile:
        async with self.session_factory() as session:
            async with session.begin():
                query = await session.execute(select(Profile).filter_by(
                    user_id=user_id).limit(1))
                profile = query.scalars().first()
                if not profile:
                    raise ProfileNotFoundError(user_id)
                return profile

    async def create_profile(
            self,
            profile: schemas.ProfileCreate,
            user_id: int
    ) -> Profile:
        async with self.session_factory() as session:
            async with session.begin():
                data = profile.dict()
                data.update({'user_id': user_id})
                db_profile = Profile(**data)
                session.add(db_profile)
                await session.commit()
                return db_profile

    async def update_profile(
            self,
            profile: schemas.ProfileCreate,
            user_id: int
    ) -> Profile:
        async with self.session_factory() as session:
            async with session.begin():
                data = profile.dict()
                query = await session.execute(select(Profile).filter_by(
                    user_id=user_id).limit(1))
                db_profile = query.scalars().first()
                for field in data:
                    setattr(db_profile, field, data[field])
                session.add(db_profile)
                await session.commit()
                return db_profile

    async def delete_profile(self, profile_id: int) -> None:
        async with self.session_factory() as session:
            async with session.begin():
                query = await session.execute(select(Profile).filter_by(
                    id=profile_id).limit(1))
                profile = query.scalars().first()
                if not profile:
                    raise ProfileNotFoundError(profile_id)
                stmt = delete(Profile).where(Profile.id == profile_id)
                await session.execute(stmt)
                await session.commit()


class ProfileNotFoundError(NotFoundError):

    entity_name: str = "Profile"
