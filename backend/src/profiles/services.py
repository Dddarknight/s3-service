from typing import Iterator

from src.profiles import schemas
from src.profiles.repositories import ProfileRepository
from src.profiles.models import Profile


class ProfileService:

    def __init__(self, profile_repository: ProfileRepository) -> None:
        self._repository: ProfileRepository = profile_repository

    async def get_profiles(self) -> Iterator[Profile]:
        return await self._repository.get_all_profiles()

    async def get_profile_by_id(self, profile_id: int) -> Profile:
        return await self._repository.get_profile_by_id(profile_id)

    async def get_profile_by_user_id(self, user_id: int) -> Profile:
        return await self._repository.get_profile_by_user_id(user_id)

    async def create_profile(
            self,
            profile: schemas.ProfileCreate,
            user_id: int
    ) -> Profile:
        return await self._repository.create_profile(profile, user_id)

    async def update_profile(
            self,
            profile: schemas.ProfileCreate,
            user_id: int
    ) -> Profile:
        return await self._repository.update_profile(profile, user_id)

    async def delete_profile(self, profile_id: int) -> None:
        return await self._repository.delete_profile(profile_id)
