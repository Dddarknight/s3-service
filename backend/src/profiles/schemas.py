from pydantic import BaseModel


class ProfileBase(BaseModel):
    address: str
    bio: str


class ProfileCreate(ProfileBase):
    pass


class Profile(ProfileBase):
    id: int
    user_id: int

    class Config:
        orm_mode = True
