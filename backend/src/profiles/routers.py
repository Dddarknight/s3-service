from typing import List, Callable
from fastapi import APIRouter, Depends, Response, Request, status
from dependency_injector.wiring import inject, Provide

from src.containers import Container
from src.profiles import schemas
from src.profiles.services import ProfileService
from src.profiles.repositories import NotFoundError
from src.profiles.exceptions import NoPermissionException


router = APIRouter()


@router.get("/profiles", response_model=List[schemas.Profile])
@inject
async def get_profiles_list(
        profile_service: ProfileService = Depends(
            Provide[Container.profile_service]),
):
    return await profile_service.get_profiles()


@router.get("/profiles/{profile_id}", response_model=schemas.Profile)
@inject
async def get_profile_by_id(
        profile_id: int,
        profile_service: ProfileService = Depends(
            Provide[Container.profile_service]),
):
    try:
        return await profile_service.get_profile_by_id(profile_id)
    except NotFoundError:
        return Response(status_code=status.HTTP_404_NOT_FOUND)


@router.post(
    "/profiles",
    response_model=schemas.Profile,
    status_code=status.HTTP_201_CREATED
)
@inject
async def create_profile(
        request: Request,
        profile: schemas.ProfileCreate,
        profile_service: ProfileService = Depends(
            Provide[Container.profile_service]),
        current_user: Callable = Depends(
            Provide[Container.current_user]),
):
    user = await current_user(request)
    return await profile_service.create_profile(profile, user.id)


@router.put(
    "/profiles/{profile_id}",
    response_model=schemas.Profile,
)
@inject
async def update_profile(
        request: Request,
        profile_id: int,
        profile: schemas.ProfileCreate,
        profile_service: ProfileService = Depends(
            Provide[Container.profile_service]),
        current_user: Callable = Depends(
            Provide[Container.current_user]),
):
    user = await current_user(request)
    db_profile = await profile_service.get_profile_by_id(profile_id)
    if db_profile.user_id == user.id:
        return await profile_service.update_profile(profile, user.id)
    raise NoPermissionException


@router.get("/my-profile", response_model=schemas.Profile)
@inject
async def get_my_profile(
        request: Request,
        profile_service: ProfileService = Depends(
            Provide[Container.profile_service]),
        current_user: Callable = Depends(
            Provide[Container.current_user]),
):
    try:
        user = await current_user(request)
        return await profile_service.get_profile_by_user_id(user.id)
    except NotFoundError:
        return Response(status_code=status.HTTP_404_NOT_FOUND)


@router.delete(
    "/profiles/{profile_id}",
    status_code=status.HTTP_204_NO_CONTENT
)
@inject
async def delete_profile(
        request: Request,
        profile_id: int,
        profile_service: ProfileService = Depends(
            Provide[Container.profile_service]),
        current_user: Callable = Depends(
            Provide[Container.current_user]),
):
    try:
        user = await current_user(request)
        if not user.profile or user.profile.id != profile_id:
            raise NoPermissionException
        await profile_service.delete_profile(profile_id)
    except NotFoundError:
        return Response(status_code=status.HTTP_404_NOT_FOUND)
    else:
        return Response(status_code=status.HTTP_204_NO_CONTENT)
