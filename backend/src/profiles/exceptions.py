from typing import Any, Dict, Optional
from fastapi import HTTPException


class NoPermissionException(HTTPException):
    def __init__(
        self,
        status_code: int = 404,
        detail: Any = "Can't change another user's profile",
        headers: Optional[Dict[str, Any]] = None,
    ):
        super().__init__(status_code=status_code, detail=detail)
