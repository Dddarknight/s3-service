import boto3
from dependency_injector import containers, providers
from fastapi.security import OAuth2PasswordBearer

from src.config import settings
from src.database import Database
from src.users.repositories import UserRepository
from src.users.services import UserService
from src.profiles.repositories import ProfileRepository
from src.profiles.services import ProfileService
from src.auth.services import AuthService
from src.files.repositories import FileRepository
from src.files.services import FileService
from src.utils import get_current_user


class Container(containers.DeclarativeContainer):

    wiring_config = containers.WiringConfiguration(
        modules=[
            "src.users.routers",
            "src.profiles.routers",
            "src.auth.routers",
            "src.files.routers",
        ],
    )

    db = providers.Singleton(Database, db_url=settings.DATABASE_URL)

    s3_client = providers.Singleton(
        boto3.client,
        service_name="s3",
        endpoint_url=settings.S3_ENDPOINT_URL,
        aws_access_key_id=settings.BOTO_KEY_ID,
        aws_secret_access_key=settings.BOTO_ACCESS_KEY,
    )

    current_token = providers.Callable(OAuth2PasswordBearer, tokenUrl="token")

    user_repository = providers.Factory(
        UserRepository,
        session_factory=db.provided.session,
    )

    user_service = providers.Factory(
        UserService,
        user_repository=user_repository,
    )

    current_user = providers.Callable(
        get_current_user,
        current_token=current_token,
        user_service=user_service
    )

    profile_repository = providers.Factory(
        ProfileRepository,
        session_factory=db.provided.session,
    )

    profile_service = providers.Factory(
        ProfileService,
        profile_repository=profile_repository,
    )

    auth_service = providers.Factory(
        AuthService,
        user_repository=user_repository,
    )

    file_repository = providers.Factory(
        FileRepository,
        session_factory=db.provided.session,
    )

    file_service = providers.Factory(
        FileService,
        file_repository=file_repository,
        s3_client=s3_client,
    )
