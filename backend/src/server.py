import os
import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles

from src.config import settings
from src.containers import Container
from src.users.routers import router as users_router
from src.profiles.routers import router as profiles_router
from src.auth.routers import router as auth_router
from src.files.routers import router as files_router


container = Container()

db = container.db()

app = FastAPI()
app.container = container
app.include_router(users_router)
app.include_router(profiles_router)
app.include_router(auth_router)
app.include_router(files_router)

app.add_middleware(
    CORSMiddleware,
    allow_origins=settings.CORS_ORIGINS,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=settings.CORS_HEADERS
)


@app.on_event('startup')
async def startup():
    await db.create_database()


if settings.MODE:
    app.mount("/", StaticFiles(
        directory=os.path.abspath(
            os.path.join(
                os.path.dirname(__file__), "../../frontend/build")
        ), html=True), name="static")


if __name__ == '__main__':
    uvicorn.run(app, host=settings.HOST, port=8000)
