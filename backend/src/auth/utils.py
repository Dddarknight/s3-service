from jose import JWTError, jwt

from src.auth.exceptions import CredentialsException
from src.auth.schemas import TokenData
from src.config import settings


def get_token(token: str):
    try:
        payload = jwt.decode(
            token,
            settings.SECRET_KEY,
            algorithms=[settings.JWT_ALGORITHM]
        )
        username: str = payload.get("sub")
        if username is None:
            raise CredentialsException
        return TokenData(username=username)
    except JWTError:
        raise CredentialsException
