from typing import Any, Dict, Optional
from fastapi import HTTPException


class CredentialsException(HTTPException):
    def __init__(
        self,
        status_code: int = 401,
        detail: Any = "Could not validate credentials",
        headers: Optional[Dict[str, Any]] = {"WWW-Authenticate": "Bearer"},
    ):
        super().__init__(
            status_code=status_code, detail=detail, headers=headers)


class ValidationException(HTTPException):
    def __init__(
        self,
        status_code: int = 401,
        detail: Any = "Incorrect username or password",
        headers: Optional[Dict[str, Any]] = {"WWW-Authenticate": "Bearer"},
    ):
        super().__init__(
            status_code=status_code, detail=detail, headers=headers)
