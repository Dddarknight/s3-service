from dependency_injector.wiring import inject, Provide
from fastapi import APIRouter, Depends
from fastapi.security import OAuth2PasswordRequestForm

from src.containers import Container
from src.auth.exceptions import ValidationException
from src.auth.services import AuthService
from src.auth.schemas import Token


router = APIRouter()


@router.post("/token", response_model=Token)
@inject
async def login_for_access_token(
        form_data: OAuth2PasswordRequestForm = Depends(),
        auth_service: AuthService = Depends(Provide[Container.auth_service]),
):

    user = await auth_service.authenticate_user(
        form_data.username, form_data.password)
    if not user:
        raise ValidationException

    access_token = auth_service.get_access_token(user)
    return {"access_token": access_token, "token_type": "bearer"}
