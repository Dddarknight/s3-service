from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from src.database import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True)
    email = Column(String, unique=True, index=True)
    full_name = Column(String)
    hashed_password = Column(String)

    profile = relationship(
        "Profile", uselist=False, back_populates="user", lazy='subquery')
    files = relationship("File", back_populates="user")
    operations = relationship("Operation", back_populates="user")
