from typing import Iterator

from src.users import schemas
from src.users.repositories import UserRepository, UserNotFoundError
from src.users.models import User
from src.users.exceptions import (
    EmailException, UsernameException
)


class UserService:

    def __init__(self, user_repository: UserRepository) -> None:
        self._repository: UserRepository = user_repository

    async def get_users(self) -> Iterator[User]:
        return await self._repository.get_all_users()

    async def get_user_by_id(self, user_id: int) -> User:
        return await self._repository.get_user_by_id(user_id)

    async def get_user_by_username(self, username: str) -> User:
        return await self._repository.get_user_by_username(username)

    async def get_user_by_email(self, email: str) -> User:
        return await self._repository.get_user_by_email(email)

    async def create_user(self, user: schemas.UserCreate) -> None:
        try:
            db_user = await self.get_user_by_username(username=user.username)
            if db_user:
                raise UsernameException
        except UserNotFoundError:
            pass
        try:
            db_user = await self.get_user_by_email(email=user.email)
            if db_user:
                raise EmailException
        except UserNotFoundError:
            pass
        return await self._repository.create_user(user)

    async def delete_user(self, user_id: int) -> None:
        return await self._repository.delete_user(user_id)
