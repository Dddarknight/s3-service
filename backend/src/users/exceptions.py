from typing import Any, Dict, Optional
from fastapi import HTTPException

from src.exceptions import NotFoundError


class NoPermissionDeleteException(HTTPException):
    def __init__(
        self,
        status_code: int = 404,
        detail: Any = "Can't delete another user",
        headers: Optional[Dict[str, Any]] = None,
    ):
        super().__init__(status_code=status_code, detail=detail)


class EmailException(HTTPException):
    def __init__(
        self,
        status_code: int = 400,
        detail: Any = 'Email already registered.',
        headers: Optional[Dict[str, Any]] = None,
    ):
        super().__init__(status_code=status_code, detail=detail)


class UsernameException(HTTPException):
    def __init__(
        self,
        status_code: int = 400,
        detail: Any = 'Username already registered.',
        headers: Optional[Dict[str, Any]] = None,
    ):
        super().__init__(status_code=status_code, detail=detail)


class UserNotFoundError(NotFoundError):

    entity_name: str = "User"
