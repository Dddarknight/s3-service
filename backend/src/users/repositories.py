from contextlib import AbstractAsyncContextManager
from typing import Callable, Iterator

from sqlalchemy import select, delete
from sqlalchemy.ext.asyncio import AsyncSession

from src.users import schemas
from src.users.exceptions import UserNotFoundError
from src.users.models import User
from src.users.utils import get_password_hash


class UserRepository:

    def __init__(
        self,
        session_factory: Callable[
            ..., AbstractAsyncContextManager[AsyncSession]],
    ) -> None:
        self.session_factory = session_factory

    async def get_all_users(self) -> Iterator[User]:
        async with self.session_factory() as session:
            async with session.begin():
                users = await session.scalars(select(User))
                return users.all()

    async def get_user_by_id(self, user_id: int) -> User:
        async with self.session_factory() as session:
            async with session.begin():
                query = await session.execute(select(User).filter_by(
                    id=user_id).limit(1))
                user = query.scalars().first()
                if not user:
                    raise UserNotFoundError(user_id)
                return user

    async def get_user_by_username(self, username: str) -> User:
        async with self.session_factory() as session:
            async with session.begin():
                query = await session.execute(select(User).filter_by(
                    username=username).limit(1))
                user = query.scalars().first()
                if not user:
                    raise UserNotFoundError(username)
                return user

    async def get_user_by_email(self, email: str) -> User:
        async with self.session_factory() as session:
            async with session.begin():
                query = await session.execute(select(User).filter_by(
                    email=email).limit(1))
                user = query.scalars().first()
                if not user:
                    raise UserNotFoundError(email)
                return user

    async def create_user(self, user: schemas.UserCreate) -> None:
        hashed_password = get_password_hash(user.password)
        async with self.session_factory() as session:
            async with session.begin():
                db_user = User(
                    username=user.username,
                    email=user.email,
                    full_name=user.full_name,
                    hashed_password=hashed_password
                )
                session.add(db_user)
                await session.commit()
                return db_user

    async def delete_user(self, user_id: int) -> None:
        async with self.session_factory() as session:
            async with session.begin():
                query = await session.execute(select(User).filter_by(
                    id=user_id).limit(1))
                user = query.scalars().first()
                if not user:
                    raise UserNotFoundError(user_id)
                stmt = delete(User).where(User.id == user_id)
                await session.execute(stmt)
                await session.commit()
