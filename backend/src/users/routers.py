from typing import List, Callable
from fastapi import APIRouter, Depends, Response, status, Request
from dependency_injector.wiring import inject, Provide

from src.containers import Container
from src.users import schemas
from src.users.services import UserService
from src.users.exceptions import UserNotFoundError, NoPermissionDeleteException


router = APIRouter()


@router.get("/users", response_model=List[schemas.User])
@inject
async def get_users(
        user_service: UserService = Depends(Provide[Container.user_service]),
):
    return await user_service.get_users()


@router.get("/users/{user_id}", response_model=schemas.User)
@inject
async def get_user_by_id(
        user_id: int,
        user_service: UserService = Depends(Provide[Container.user_service]),
):
    try:
        return await user_service.get_user_by_id(user_id)
    except UserNotFoundError:
        return Response(status_code=status.HTTP_404_NOT_FOUND)


@router.post(
    "/users",
    response_model=schemas.User,
    status_code=status.HTTP_201_CREATED
)
@inject
async def create_user(
        user: schemas.UserCreate,
        user_service: UserService = Depends(Provide[Container.user_service]),
):
    return await user_service.create_user(user)


@router.delete("/users/{user_id}", status_code=status.HTTP_204_NO_CONTENT)
@inject
async def delete_user(
        user_id: int,
        request: Request,
        user_service: UserService = Depends(Provide[Container.user_service]),
        current_user: Callable = Depends(
            Provide[Container.current_user]),
):
    try:
        user = await current_user(request)
        if user.id != user_id:
            raise NoPermissionDeleteException
        await user_service.delete_user(user_id)
    except UserNotFoundError:
        return Response(status_code=status.HTTP_404_NOT_FOUND)
    else:
        return Response(status_code=status.HTTP_204_NO_CONTENT)
