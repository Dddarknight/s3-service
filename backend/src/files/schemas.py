import datetime
from enum import Enum

from pydantic import BaseModel


class FileBase(BaseModel):
    name: str
    bucket: str


class FileCreate(FileBase):
    pass


class File(FileBase):
    id: int
    user_id: int

    class Config:
        orm_mode = True


class OperationType(str, Enum):
    UPLOAD = "UPLOAD"
    LOAD = "LOAD"


class Operation(BaseModel):
    id: int
    created_at: datetime.datetime
    file: File
    user_id: int
    operation_type: OperationType = None

    class Config:
        orm_mode = True


class Link(BaseModel):
    link: str
