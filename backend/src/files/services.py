from typing import Iterator
from mypy_boto3_s3 import S3Client
from fastapi import File, UploadFile

from src.config import settings
from src.files import schemas
from src.files.repositories import FileRepository
from src.files.models import Operation, File as File_model


class FileService:

    def __init__(
            self,
            file_repository: FileRepository,
            s3_client: S3Client,
    ) -> None:
        self._repository: FileRepository = file_repository
        self._s3_client = s3_client

    async def get_presigned_url(self, name, bucket):
        presigned_url = self._s3_client.generate_presigned_url(
            'get_object',
            Params={'Bucket': bucket, 'Key': name},
            ExpiresIn=settings.FILE_LOAD_LINK_EXPIRE_TIME
        )
        return {'link': presigned_url}

    async def upload_file(
            self,
            file: schemas.FileCreate,
            user_id: int,
            file_obj: UploadFile = File(...),
    ) -> schemas.Link:
        try:
            self._s3_client.create_bucket(Bucket=file.bucket)
        except Exception:
            pass
        self._s3_client.upload_fileobj(
            file_obj.file, file.bucket, file.name)
        await self._repository.upload_file(user_id, file)
        return await self.get_presigned_url(file.name, file.bucket)

    async def load_file(
            self,
            file_id: int,
            user_id: int,
    ) -> str:
        await self._repository.load_file(file_id, user_id)
        file = await self.get_file_by_id(file_id)
        return await self.get_presigned_url(file.name, file.bucket)

    async def get_files(self, prefix: str) -> Iterator[File_model]:
        return await self._repository.get_files(prefix)

    async def get_file_by_id(self, file_id: int) -> File_model:
        return await self._repository.get_file_by_id(file_id)

    async def get_operations(self, user_id: int) -> Iterator[Operation]:
        return await self._repository.get_operations(user_id)
