from contextlib import AbstractContextManager
from typing import Callable, Iterator

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from src.exceptions import NotFoundError
from src.files import schemas
from src.files.models import File, Operation


class FileRepository:

    def __init__(
        self,
        session_factory: Callable[..., AbstractContextManager[AsyncSession]],
    ) -> None:
        self.session_factory = session_factory

    async def upload_file(self, user_id, file: schemas.FileCreate) -> File:
        async with self.session_factory() as session:
            async with session.begin():
                db_file = File(
                    name=file.name,
                    bucket=file.bucket,
                    user_id=user_id,
                )
                session.add(db_file)
                await session.commit()

            async with session.begin():
                operation = Operation(
                    operation_type='UPLOAD',
                    file_id=db_file.id,
                    user_id=user_id
                )
                session.add(operation)
                await session.commit()
                return db_file

    async def load_file(self, file_id, user_id) -> Operation:
        async with self.session_factory() as session:
            async with session.begin():
                operation = Operation(
                    operation_type='LOAD',
                    file_id=file_id,
                    user_id=user_id
                )
                session.add(operation)
                await session.commit()
                return operation

    async def get_file_by_id(self, file_id: int) -> File:
        async with self.session_factory() as session:
            async with session.begin():
                query = await session.execute(select(File).filter_by(
                    id=file_id).limit(1))
                file = query.scalars().first()
                if not file:
                    raise FileNotFoundError(file_id)
                return file

    async def get_files(self, prefix) -> Iterator[File]:
        async with self.session_factory() as session:
            async with session.begin():
                if not prefix:
                    files = await session.scalars(select(File))
                    return files.all()
                files = await session.execute(select(File).where(
                    File.name.like(f'{prefix}%')))
                return files.scalars().all()

    async def get_operations(self, user_id) -> Iterator[Operation]:
        async with self.session_factory() as session:
            async with session.begin():
                query = await session.execute(select(Operation).where(
                    Operation.user_id == user_id))
                operations = query.scalars().all()
                return operations


class FileNotFoundError(NotFoundError):

    entity_name: str = "File"
