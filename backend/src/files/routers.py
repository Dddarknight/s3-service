from typing import List, Callable
from fastapi import (
    APIRouter, Depends, status, Request, File, UploadFile, Form
)
from dependency_injector.wiring import inject, Provide

from src.containers import Container
from src.files import schemas
from src.files.services import FileService


router = APIRouter()


@router.post(
    "/files",
    response_model=schemas.Link,
    status_code=status.HTTP_201_CREATED
)
@inject
async def upload_file(
        request: Request,
        name: str = Form(...),
        bucket: str = Form(...),
        file_obj: UploadFile = File(...),
        file_service: FileService = Depends(Provide[Container.file_service]),
        current_user: Callable = Depends(
            Provide[Container.current_user]),
):
    file = schemas.FileCreate(**({'name': name, 'bucket': bucket}))
    user = await current_user(request)
    return await file_service.upload_file(file, user.id, file_obj)


@router.get(
    "/files/{file_id}",
    response_model=schemas.Link
)
@inject
async def load_file(
        request: Request,
        file_id: int,
        file_service: FileService = Depends(Provide[Container.file_service]),
        current_user: Callable = Depends(
            Provide[Container.current_user]),
):
    user = await current_user(request)
    return await file_service.load_file(file_id, user.id)


@router.get("/files", response_model=List[schemas.File])
@inject
async def get_files(
        request: Request,
        file_service: FileService = Depends(Provide[Container.file_service]),
        prefix: str = '',
        current_user: Callable = Depends(
            Provide[Container.current_user]),
):
    await current_user(request)
    return await file_service.get_files(prefix)


@router.get("/operations", response_model=List[schemas.Operation])
@inject
async def get_operations(
        request: Request,
        file_service: FileService = Depends(Provide[Container.file_service]),
        current_user: Callable = Depends(
            Provide[Container.current_user]),
):
    user = await current_user(request)
    return await file_service.get_operations(user.id)
