from fastapi import Request

from src.auth.utils import get_token
from src.config import settings
from src.users.models import User
from src.users.services import UserService


async def get_current_user(
        user_service: UserService,
        current_token: str
) -> User:
    async def inner(request: Request):
        current_token_str = await current_token(request)
        username = get_token(current_token_str).username if (
            settings.OAUTH2_METHOD == 'JWT') else current_token_str
        user = await user_service.get_user_by_username(username)
        return user
    return inner
