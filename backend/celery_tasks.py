import asyncio
import contextlib
import io
import os
import zipfile
from celery import Celery
from celery import shared_task
from celery.schedules import crontab
from dependency_injector.wiring import inject, Provide
from mypy_boto3_s3 import S3Client

from src.config import settings
from src.containers import Container


task_app = Celery(
    __name__,
    backend=settings.BROKER_URL,
    broker=settings.BROKER_URL,
    task_track_started=True,
    task_ignore_result=False,
    enable_utc=True
)

task_app.autodiscover_tasks()


@inject
async def download_file(
    bucket,
    file_name,
    s3_client: S3Client = Provide[Container.s3_client],
):
    file = io.BytesIO(b"")
    s3_client.download_fileobj(bucket, file_name, file)
    return {
        'file_name': file_name,
        'content': file
    }


@inject
async def download_files(
    s3_client: S3Client = Provide[Container.s3_client],
):
    tasks = []
    buckets = [
        bucket['Name'] for bucket in s3_client.list_buckets()['Buckets']]

    for bucket in buckets:
        objects = s3_client.list_objects(Bucket=bucket)
        try:
            objects_contents = objects['Contents']
        except Exception:
            continue
        for object in objects_contents:
            file_name = object['Key']
            task = asyncio.ensure_future(
                download_file(bucket, file_name)
            )
            tasks.append(task)
    files = await asyncio.gather(*tasks, return_exceptions=True)
    return files


@shared_task()
def task_get_s3_archive():

    container = Container()
    container.wire(modules=[__name__])

    with contextlib.suppress(FileNotFoundError):
        os.remove(f'{settings.FILES_DIRECTORY}.zip')

    files = asyncio.get_event_loop().run_until_complete(
        download_files())
    archive = zipfile.ZipFile(f'{settings.FILES_DIRECTORY}.zip', 'w')
    for file in files:
        archive.writestr(file.get('file_name'), file.get('content').read())


task_app.conf.beat_schedule = {
    'get_s3_archive': {
        'task': 'celery_tasks.task_get_s3_archive',
        'schedule': crontab(minute=f"*/{settings.PERIOD_FOR_CELERY_TASK}")
    }
}
